# Digital Landscapes in Interdisciplinary Mountain Research

**Unearthing Insights in Earth System Sciences through Cross-Fertilization of Knowledge and Technologies**

Public presentation on 29 July 2022 at the [Institute for Interdisciplinary Mountain Research](https://www.oeaw.ac.at/en/igf/home) (IGF), Austrian Academy of Sciences (ÖAW), Innsbruck, Austria.

The slides are created using [Quarto](https://quarto.org/)-Presentation with a [Reveal.js](https://revealjs.com/) backend. The presentation (HTML) is served through [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/) using a simple CI/CD pipeline.
