---
title: "Digital Landscapes in Interdisciplinary Mountain Research"
subtitle: "Unearthing Insights in Earth System Sciences through<br/>Cross-Fertilization of Knowledge and Technologies"
format:
  revealjs:
    theme: [dark, custom.scss]
    # https://github.com/quarto-dev/quarto-cli/blob/main/src/resources/formats/revealjs/themes/dark.scss
    slide-number: true
    reference-location: document
    preview-links: auto
    logo: https://www.oeaw.ac.at/fileadmin/oeaw/institutstemplate/igf/img/IGF_Logo.png
    #logo: https://www.oeaw.ac.at/typo3conf/ext/oeaw_redesign/Resources/Public/Icons/logo-kurz.png
    footer: "Matthias Schlögl"
    css: styles.css
    chalkboard:
      buttons: false
#resources:
#  - demo.pdf
---


# Digital Landscapes

::: {layout-ncol="3"}
![](https://www.rayshader.com/reference/figures/smallhobart.gif)
<!-- Source: [Tyler Morgan-Wall (2022): rayshader](https://www.rayshader.com/)-->
![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/LonelyMountain.jpg/1024px-LonelyMountain.jpg)
<!-- Source: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:LonelyMountain.jpg) -->

![](https://www.esa.int/var/esa/storage/images/esa_multimedia/images/2021/10/la_palma_as_captured_by_copernicus_sentinel-2/23513153-2-eng-GB/La_Palma_as_captured_by_Copernicus_Sentinel-2_pillars.jpg)
<!-- Source: [ESA](https://www.esa.int/ESA_Multimedia/Images/2021/10/La_Palma_as_captured_by_Copernicus_Sentinel-2) -->

![](https://upload.wikimedia.org/wikipedia/commons/7/7d/Mars_atmosphere.jpg)

<!-- Source: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Mars_atmosphere.jpg) -->
:::


## There and back again {background-image="https://cdnb.artstation.com/p/assets/images/images/000/216/523/large/bastien-grivet-caradhras.jpg" .smaller}
<!-- [Grivet, Bastien: Caradhras](https://www.artstation.com/artwork/qAENy) -->

::: {style="background-color: rgba(25, 25, 25, .7)"}
> The real voyage of discovery consists not in seeking new landscapes,<br/>but in having new eyes.
> 
> `r tufte::quote_footer('--- Marcel Proust (1923): *À la Recherche du temps perdu: La Prisonnière*.')`

<!-- The only true voyage of discovery, the only fountain of Eternal Youth, would be not to visit strange lands but to possess other eyes, to behold the universe through the eyes of another, of a hundred others, to behold the hundred universes that each of them beholds, that each of them is; and this we can contrive with an Elstir, with a Vinteuil; with men like these we do really fly from star to star. -->
:::


# The voyage so far


## Transportation

::: {layout-ncol="3" style="text-align: center; font-size: 0.5em;"}
![](https://upload.wikimedia.org/wikipedia/commons/b/b7/Felbertauern_Felssturz.JPG)
**Rockfall** on 2013-05-14.<br/>
Schildalmgalerie,<br/>B108 (Felbertauern Straße),<br/>Matrei in Osttirol.
<!--  Source: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Felbertauern_Felssturz.JPG)-->

![](https://www.swr.de/swraktuell/rheinland-pfalz/koblenz/1626625138759,bruecke-ahrweiler-zerstoert-100~_v-16x9@2dL_-6c42aff4e68b43c7868c3240d3ebfa29867457da.jpg)
<!-- Source: [SWR](https://www.swr.de/swraktuell/rheinland-pfalz/koblenz/)-->
**Flooding** on 2021-07-15.<br/>
Ahrweiler, Rheinland-Pfalz.
![](https://media1.faz.net/ppmedia/aktuell/gesellschaft/4117621719/1.2294931/format_top1_breit/gefaehrliche-strassenschaeden.jpg)
<!--  Source: [FAZ](https://www.faz.net/aktuell/gesellschaft/ungluecke/strassenschaeden-explodierender-beton-12294568.html) -->
**Concrete slab blow-ups** on 2013-07-23.<br/>
A14, Halle (Saale), Sachsen-Anhalt.

![](https://media04.meinbezirk.at/article/2021/02/03/1/25348461_L.jpg?1612371779)
**Snow avalanche** on 2021-02-03.<br/>
Pitztalstraße, St.Leonhard im Pitztal.
<!-- Source: [Bezirksblatt Imst](https://www.meinbezirk.at/imst/c-lokales/lawine-ging-auf-die-pitztalstrasse-ab_a4467276) -->
:::


## Transportation Infrastructure {.smaller}

::: {.fragment .fade-in-then-semi-out}
- What are extreme events, and how can we assess them?^[**Schlögl, M.** & Laaha, G. (2017): Extreme weather exposure identification for road networks – a comparative assessment of statistical methods. *Nat. Hazards Earth Syst. Sci.*, 17, 515–531. [doi:10.5194/nhess-17-515-2017](https://doi.org/10.5194/nhess-17-515-2017).]
:::

::: {.fragment .fade-in}
![](./img/eva.png){.absolute top=360 left=0 width="248" height="270"}
:::

::: {.fragment .fade-in-then-semi-out .fade-up}
- Climate change: What is the potential future exposure of critical infrastructure to extremes of certain types?^[**Schlögl, M.** & Matulla, C. (2018): Potential future exposure of European land transport infrastructure to rainfall-induced landslides throughout the 21st century. *Nat. Hazards Earth Syst. Sci.*, 18, 1121–1132. [doi:10.5194/nhess-18-1121-2018](https://doi.org/10.5194/nhess-18-1121-2018).]
:::

::: {.fragment .fade-in}
![](./img/landslide_days_exposure_ff.jpg){.absolute top=360 left=270 width="270" height="270"}
:::

::: {.fragment .fade-in-then-semi-out .fade-up}
- Impacts: How do network interruptions affect the local population?^[**Schlögl, M.**, Richter, G., Avian, M., Thaler, T., Heiss, G., Lenz, G. & Fuchs, S. (2019): On the nexus between landslide susceptibility and transport infrastructure – an agent-based approach. *Nat. Hazards Earth Syst. Sci.*, 19, 201–219. [doi:10.5194/nhess-19-201-2019](https://doi.org/10.5194/nhess-19-201-2019).]
:::

::: {.fragment .fade-in}
![](https://nhess.copernicus.org/articles/19/201/2019/nhess-19-201-2019-f05-web.png){.absolute top=360 left=562 width="396" height="270"}
:::


## Impacts of Interruptions {.smaller}

::: columns
::: {.column width="50%"}
<iframe seamless src="./html/nhess-19-201-2019-s1_susceptibility_map/supplement-1-map.html" width="500" height="550"></iframe>
:::

::: {.column width="50%"}
<iframe seamless src="./html/nhess-19-201-2019-s2_affected_agents_map/supplement-2-map.html" width="500" height="550"></iframe>
:::
:::


## Road Safety {.smaller}

::: {.fragment .fade-in-then-semi-out .fade-up}
- How accurate are available data sets, particularly in space and time?^[**Schlögl, M.** & Stütz, R. (2017): Methodological considerations with data uncertainty in road safety analysis. *Accid. Anal. Prev.*, 130, 136-150. [doi:10.1016/j.aap.2017.02.001](https://doi.org/10.1016/j.aap.2017.02.001).]
:::

::: {.fragment .fade-in}
![](./img/accident_times.png){.absolute top=400 left=0 width="410" height="200"}
:::

::: {.fragment .fade-in-then-semi-out .fade-up}
- How can we deal with severe class imbalance^[**Schlögl, M.** (2020): A multivariate analysis of environmental effects on road accident occurrence using a balanced bagging approach. *Accid. Anal. Prev.*, 136, 105398. [doi:10.1016/j.aap.2019.105398](https://doi.org/10.1016/j.aap.2019.105398).]
:::

::: {.fragment .fade-in}
![](./img/balanced_bagging.png){.absolute top=400 left=435 width="282" height="200"}
:::

::: {.fragment .fade-in-then-semi-out .fade-up}
- How do different accident prediction models perform?^[**Schlögl, M.**, Stütz, R., Laaha, G. & Melcher, M. (2019): A comparison of statistical learning methods for deriving determining factors of accident occurrence from an imbalanced high resolution dataset. *Accid. Anal. Prev.*, 127, 134-149. [doi:10.1016/j.aap.2019.02.008](https://doi.org/10.1016/j.aap.2019.02.008).]
:::

::: {.fragment .fade-in}
![](./img/prediction_probabilites.png){.absolute top=135 left=854 width="188" height="240"}

![](./img/aadt_pdp.png){.absolute top=400 left=742 width="300" height="200"}
:::


## Vulnerability {.smaller}

::: {.fragment .fade-in-then-semi-out .fade-up}
- Vulnerability curves:
    - Mapping degree of loss and process intensity^[Fuchs, S., Heiser, M., **Schlögl, M.**, Zischg, A., Papathoma-Köhle, M. & Keiler, M. (2019): Short communication: A model to predict flood loss in mountain areas. *Environ. Modell. Softw.*, 117, 176-180. [doi:10.1016/j.envsoft.2019.03.026](https://doi.org/10.1016/j.envsoft.2019.03.026).]
    - Expert versus data-driven flood damage models^[Malgwi, M. B., **Schlögl, M.**, & Keiler, M. (2021): Expert-based versus data-driven flood damage models: a comparative evaluation for data-scarce regions. *Int J. Disast. Risk Re.*, 57, 102148. [doi:10.1016/j.ijdrr.2021.102148](https://doi.org/10.1016/j.ijdrr.2021.102148).]
:::

::: {.fragment .fade-in}
![](https://ars.els-cdn.com/content/image/1-s2.0-S1364815219301227-gr2_lrg.jpg){.absolute top=100 left=730 width="270" height="225"}
:::

::: {.fragment .fade-in-then-semi-out .fade-up}
- Vulnerability Indicators:
    - Selection and weighting: dynamic flooding^[Papathoma-Köhle, M., **Schlögl, M.** & Fuchs, S. (2019): Vulnerability indicators for natural hazards: an innovative selection and weighting approach. *Sci. Rep.*, 9, 15026. [doi:10.1038/s41598-019-50257-2](https://doi.org/10.1038/s41598-019-50257-2).]
    - Vulnerability index for wildfires^[Papathoma-Köhle, M., **Schlögl, M.**, Garlichs, C., Diakakis, M., Mavroulis, S. & Fuchs, S. (2022): A wildfire vulnerability index for buildings. *Sci. Rep.*, 12, 6378. [doi:10.1038/s41598-022-10479-3](https://doi.org/10.1038/s41598-022-10479-3).]
    - Validating vulnerability curves and indices^[Papathoma-Köhle, M., **Schlögl, M.**, Dosser, L., Roesch, F., Borga, M., Erlicher, M., Keiler, M. & Fuchs, S. (2022): Physical vulnerability of buildings to dynamic flooding: vulnerability curves and vulnerability indices. *J. Hydrol.*, 607, 127501. [doi:10.1016/j.jhydrol.2022.127501](https://doi.org/10.1016/j.jhydrol.2022.127501).]
:::

::: {.fragment .fade-in}
![](https://media.springernature.com/full/springer-static/image/art%3A10.1038%2Fs41598-019-50257-2/MediaObjects/41598_2019_50257_Fig2_HTML.png){.absolute top=395 left=661 width="338" height="225"}
:::

::: {.fragment .fade-in-then-semi-out .fade-up}
- Risk communication^[Attems, M.-S., **Schlögl, M.**, Thaler, T., Rauter, M. & Fuchs, S. (2020): Risk communication and adaptive behaviour in flood-prone areas of Austria: A Q-methodology study on opinions of affected homeowners. *PLoS ONE*, 15(5), e0233551. [doi:10.1371/journal.pone.0233551](https://doi.org/10.1371/journal.pone.0233551).]
:::


## Earth Observation {.smaller}

::: {.incremental}

- Deriving ISO-defined sustainability indicators via remote sensing^[Lehner, A., Erlacher, C., **Schlögl, M.**, Wegerer, J., Blaschke, T. & Steinnocher, K. (2018): Can ISO-defined urban sustainability indicators be derived from remote sensing: an expert weighting approach. *Sustainability*, 10(4), 1268. [doi:10.3390/su10041268](https://doi.org/10.3390/su10041268).]

- Status of earth observation techniques in monitoring high mountain environments^[Avian, M., Bauer, C., **Schlögl, M.**, Widhalm, B., Gutjahr, K.-H., Paster, M., Hauer, C., Frießenbichler, M., Neureiter, A., Weyss, G., Flödl, P., Seier, G., Sulzer, W. (2020): The Status of Earth Observation Techniques in Monitoring High Mountain Environments at the Example of Pasterze Glacier, Austria: Data, Methods, Accuracies, Processes, and Scales. *Remote Sens.*, 12, 1251. [doi:10.3390/rs120812518](https://doi.org/10.3390/rs120812518).]

- Challenges in using InSAR for early warning applications^[**Schlögl, M.**, Gutjahr, K. & Fuchs, S. (2022): The challenge to use multi-temporal InSAR for landslide early warning. *Nat. Hazards*. [doi:10.1007/s11069-022-05289-9](https://doi.org/10.1007/s11069-022-05289-9). Repository: <https://gitlab.com/Rexthor/insar-early-warning>.]

- Surface deformation monitoring using InSAR^[**Schlögl, M.**, Widhalm, B., & Avian, M. (2021): Comprehensive time-series analysis of bridge deformation using differential satellite radar interferometry based on Sentinel-1. *ISPRS J. Photogramm. Remote Sens.*, 172, 132-146. [doi:10.1016/j.isprsjprs.2020.12.001](https://doi.org/10.1016/j.isprsjprs.2020.12.001).] and other remote sensing methods^[**Schlögl, M.**, Dorninger, P., Kwapisz, M., Ralbovsky, M. & Spielhofer, R. (2022): Remote Sensing Techniques for Bridge Deformation Monitoring at Millimetric Scale: Investigating the Potential of Satellite Radar Interferometry, Airborne Laser Scanning and Ground-Based Mobile Laser Scanning. *PFG - J. Photogramm. Remote Sens. Geoinf. Sci.* [doi:10.1007/s41064-022-00210-2](https://doi.org/10.1007/s41064-022-00210-2). Supplement:
[doi:10.6084/m9.figshare.20035364](https://doi.org/10.6084/m9.figshare.20035364).]

:::

::: {.fragment .fade-in}
![](https://www.mdpi.com/remotesensing/remotesensing-12-01251/article_deploy/html/images/remotesensing-12-01251-g001.png){.absolute top=350 left=0 width="214" height="280"}

![](https://ars.els-cdn.com/content/image/1-s2.0-S0924271620303324-gr7_lrg.jpg){.absolute top=350 left=229 width="525" height="280"}

![](https://media.springernature.com/full/springer-static/image/art%3A10.1007%2Fs11069-022-05289-9/MediaObjects/11069_2022_5289_Fig1_HTML.png){.absolute top=350 left=768 width="338" height="280"}
:::


## Torrential Flooding {.smaller}

::: {.incremental}

- Trigger patterns for natural hazards^[Enigl, K., Matulla, C., **Schlögl, M.** & Schmid, F. (2019): Derivation of canonical total-sequences triggering landslides and floodings in complex terrain. *Adv. Water Resour.*, 129, 178-188. [doi:10.1016/j.advwatres.2019.04.018](https://doi.org/10.1016/j.advwatres.2019.04.018).]

- Trends in torrential flooding^[**Schlögl, M.**, Fuchs, S.. Scheidl, C. & Heiser, M. (2021): Trends in torrential flooding in the Austrian Alps: A combination of climate change, exposure dynamics, and mitigation measures. *Clim. Risk Manag.*, 32, 100294. [doi:10.1016/j.crm.2021.100294](https://doi.org/10.1016/j.crm.2021.100294).]

- Fallacies in trend analysis^[Heiser, M., **Schlögl, M.**, Scheidl, C. & Fuchs, S. (2022): Fallacies in debris flow trend analysis. *J. Geophys. Res. Earth Surf.*, 127(3), e2021JF006562. [doi:10.1029/2021JF006562](https://doi.org/10.1029/2021JF006562). Repository: <https://gitlab.com/Rexthor/debris-flow-trends-zermatt>.]

:::

::: {.fragment .fade-in}
![](https://ars.els-cdn.com/content/image/1-s2.0-S2212096321000231-gr1_lrg.jpg){.absolute top=300 left=20 width="300" height="150"}
![](https://ars.els-cdn.com/content/image/1-s2.0-S2212096321000231-gr2_lrg.jpg){.absolute top=300 left=345 width="300" height="150"}
![](https://ars.els-cdn.com/content/image/1-s2.0-S2212096321000231-gr3_lrg.jpg){.absolute top=300 left=670 width="300" height="150"}

![](https://ars.els-cdn.com/content/image/1-s2.0-S2212096321000231-gr4_lrg.jpg){.absolute top=475 left=20 width="300" height="150"}
![](https://ars.els-cdn.com/content/image/1-s2.0-S2212096321000231-gr5_lrg.jpg){.absolute top=475 left=345 width="300" height="150"}
![](https://ars.els-cdn.com/content/image/1-s2.0-S2212096321000231-gr6_lrg.jpg){.absolute top=475 left=670 width="300" height="150"}

![](https://agupubs.onlinelibrary.wiley.com/cms/asset/62ccaca3-11c3-4342-a54d-4e8e449ce5ed/jgrf21518-fig-0003-m.jpg){.absolute top=175 left=487 width="483" height="100"}

:::


# The journey ahead


## Ignoramus et ignorabimus? {.smaller}

::: {style="margin-top: 120px;"}
> Wir dürfen nicht denen glauben, die heute mit philosophischer Miene und überlegenem Tone den Kulturuntergang prophezeien und sich in dem *Ignorabimus* gefallen. Für uns gibt es kein *Ignorabimus*, und meiner Meinung nach auch für die Naturwissenschaft überhaupt nicht. Statt des törichten *Ignorabimus* heiße im Gegenteil unsere Losung:<br/>
> Wir müssen wissen,<br/>
> Wir werden wissen.
> 
> `r tufte::quote_footer('--- David Hilbert (1930). [Radio Address](https://www.swr.de/swr2/wissen/archivradio/aexavarticle-swr-42234.html).')`
:::


## Trends in Torrential Flooding {.smaller}

::: {.fragment .fade-in-then-semi-out}
- Generic introduction: *"Climate change leads to an increase in frequency and magnitude of natural hazard events."*
- But can we show changes in **frequency**, **magnitude** and (seasonal) **timing** of damage-inducing torrential events?
- Can this be attributed to **climate change**?
- How can we assess **exposure**?
- How can we assess **mitigation**?
:::

::: {.fragment .fade-in}
::: {.fragment .highlight-blue}
- Which parameters influence event occurrence on a **regional** scale?
:::
:::


## Digital Landscapes {.smaller}

::: {.panel-tabset}

### Events {.smaller}

![](https://ars.els-cdn.com/content/image/1-s2.0-S2212096321000231-gr11_lrg.jpg){fig-align="center"}

::: columns
::: {.column width="50%"}
::: {style="text-align: center; font-size: 0.75em"}
Fluvial sediment transport (1962-2017)
:::
:::

::: {.column width="50%"}
::: {style="text-align: center; font-size: 0.75em"}
Debris flows (1962-2017)
:::
:::

:::


### Mitigation {.smaller}

![](https://ars.els-cdn.com/content/image/1-s2.0-S2212096321000231-gr12_lrg.jpg){fig-align="center"}

::: columns
::: {.column width="60%"}
::: {style="text-align: center; font-size: 0.75em"}
Total number of torrent control structures solely<br/>
based on the *year of construction* (dashed) and<br/>´
number of functional structures (solid).
:::
:::

::: {.column width="40%"}
::: {style="text-align: center; font-size: 0.75em"}
Number of structures constructed (solid) and number of structures whose *average lifetime* was exceeded in that year (dashed).
:::
:::
:::

### Exposure {.smaller}

![](https://ars.els-cdn.com/content/image/1-s2.0-S2212096321000231-gr13_lrg.jpg){fig-align="center"}

::: columns
::: {.column width="60%"}
::: {style="text-align: center; font-size: 0.75em"}
Number of exposed buildings.
:::
:::

::: {.column width="40%"}
::: {style="text-align: center; font-size: 0.75em"}
Number of exposed buildings per functional torrent control structures.
:::
:::
:::


### Climate {.smaller}

- Climatologic datasets (e.g. temperature, precipitation, evapotranspiration, snow)^[Open Data / PSI directive (e.g. <https://data.hub.zamg.ac.at/>)]
- Climate indicators signifying precursor conditions / drivers
- Two full climate normals (1962-2022)

<iframe seamless src="https://data.hub.zamg.ac.at/" width="1000" height="300"></iframe>

### Other precursors {.smaller}

- Topography / geomorphometry
- Land use changes (particularly: forest cover)

:::


# Integration in IGF

## Research Groups {auto-animate=true auto-animate-easing="ease-in-out"}

::: {.r-stack}
::: {data-id="igf_ag_1" style="margin-top: 25%; margin-left: 20%; background: #1b9e77; width: 20px; height: 20px; opacity: 0.9; opacity: 0.25"}
:::

::: {data-id="igf_ag_2" style="margin-top: 10%; margin-left: 50%; background: #d95f02; width: 20px; height: 20px; opacity: 0.9; opacity: 0.25"}
:::

::: {data-id="igf_ag_3" style="margin-top: 50%; margin-left: 60%; background: #7570b3; width: 20px; height: 20px; opacity: 0.9; opacity: 0.25"}
:::

::: {data-id="igf_ag_4" style="margin-top: -5%; margin-left: 80%; background: #e7298a; width: 20px; height: 20px; opacity: 0.9; opacity: 0.25"}
:::
:::

## Research Groups {auto-animate=true auto-animate-easing="ease-in-out"}

::: {.r-hstack}
::: {data-id="igf_ag_1" style="background: #1b9e77; width: 200px; height: 150px; margin: 20px;"}
:::

::: {data-id="igf_ag_2"  style="background: #d95f02; width: 200px; height: 150px; margin: 20px;"}
:::

::: {data-id="igf_ag_3" style="background: #7570b3; width: 200px; height: 150px; margin: 20px;"}
:::

::: {data-id="igf_ag_4" style="background: #e7298a; width: 200px; height: 150px; margin: 20px;"}
:::
:::

::: {.r-hstack}
::: {style="color: #1b9e77; width: 200px; height: 150px; margin: 20px; text-align: center; font-size: 0.75em"}
Man and Environment:<br/>Settlements
:::

::: {style="color: #d95f02; width: 200px; height: 150px; margin: 20px; text-align: center; font-size: 0.75em"}
Man and Environment:<br/>Highlands
:::

::: {style="color: #7570b3; width: 200px; height: 150px; margin: 20px; text-align: center; font-size: 0.75em"}
Remote Sensing and Geomatics
:::

::: {style="color: #e7298a; width: 200px; height: 150px; margin: 20px; text-align: center; font-size: 0.75em"}
GLORIA
:::
:::


## Quo vadis? {auto-animate=true auto-animate-easing="ease-in-out"}

::: {.r-hstack}
::: {data-id="igf_ag_1" auto-animate-delay="0" style="background: #1b9e77; width: 200px; height: 150px; margin: 20px; opacity: 0.5;"}
:::

::: {data-id="igf_ag_2" auto-animate-delay="0.1" style="background: #d95f02; width: 200px; height: 150px; margin: 20px; opacity: 0.5;"}
:::

::: {data-id="igf_ag_3" auto-animate-delay="0.2" style="background: #7570b3; width: 200px; height: 150px; margin: 20px; opacity: 0.5;"}
:::

::: {data-id="igf_ag_4" auto-animate-delay="0.4" style="background: #e7298a; width: 200px; height: 150px; margin: 20px; opacity: 0.5;"}
:::

::: {data-id="igf_ag_5" auto-animate-delay="0.5" style="background: #66a61e; width: 200px; height: 150px; margin: 20px;"}
:::
:::

::: {.fragment .fade-in-then-out style="margin-top: 80px; margin-left: 680px; color: #66a61e; transform: rotate(-40deg);"}
Digital Landscapes
:::

::: {.fragment .fade-in style="margin-top: -265px; margin-left: 940px; color: rgb(25, 25, 25);"}
?
:::

## Quo vadis? {auto-animate=true auto-animate-easing="ease-in-out"}

::: {.r-vstack}
::: {data-id="igf_ag_1" style="background: #1b9e77; width: 100px; height: 100px; border-radius: 200px; margin: 10px;"}
:::

::: {data-id="igf_ag_2" style="background: #d95f02; width: 100px; height: 100px; border-radius: 200px; margin: 10px;"}
:::

::: {data-id="igf_ag_3" style="background: #7570b3; width: 100px; height: 100px; border-radius: 200px; margin: 10px;"}
:::

::: {data-id="igf_ag_4" style="background: #e7298a; width: 100px; height: 100px; border-radius: 200px; margin: 10px;"}
:::

::: {data-id="igf_ag_5" style="background: #66a61e; width: 100px; height: 100px; border-radius: 200px; margin: 10px;"}
:::
:::

## Integration {auto-animate=true auto-animate-easing="ease-in-out"}

::: {.r-stack}

::: {data-id="igf_ag_5" style="background: #66a61e; width: 500px; height: 500px; border-radius: 100px; margin-top: 50px; margin-left: 275px; border-color: rgba(25, 25, 25, .7); border: solid; border-width: 3px;"}
:::

::: {data-id="igf_ag_1" style="background: #1b9e77; width: 150px; height: 150px; border-radius: 100px; margin-top: 125px; margin-left: 350px; border-color: rgba(25, 25, 25, .7); border: dashed; border-width: 2px;"}
:::

::: {data-id="igf_ag_2" style="background: #d95f02; width: 150px; height: 150px; border-radius: 100px; margin-top: 125px; margin-left: 550px; border-color: rgba(25, 25, 25, .7); border: dashed; border-width: 2px;"}
:::

::: {data-id="igf_ag_3" style="background: #7570b3; width: 150px; height: 150px; border-radius: 100px; margin-top: 325px; margin-left: 350px; border-color: rgba(25, 25, 25, .7); border: dashed; border-width: 2px;"}
:::

::: {data-id="igf_ag_4" style="background: #e7298a; width: 150px; height: 150px; border-radius: 100px; margin-top: 325px; margin-left: 550px; border-color: rgba(25, 25, 25, .7); border: dashed; border-width: 2px;"}
:::
:::

::: {.fragment}

::: {.absolute top=10% left=-10% style="color: #1b9e77; font-size: 0.7em;"}
Man & Environment<br/>Settlements:

- human-environment<br/>interactions
- natural and cultural<br/>landscapes
- land-use changes
:::

::: {.absolute top=10% left=80% style="color: #d95f02; font-size: 0.7em;"}
Man & Environment<br/>Highlands:

- geomorphometry
- hydrology
- cryosphere
- time series analysis
:::

::: {.absolute top=60% left=-10% style="color: #7570b3; font-size: 0.7em;"}
Remote Sensing & Geomatics:

- earth observation<br/>(multispectral, SAR)
- time series analysis
- monitoring
:::

::: {.absolute top=60% left=80% style="color: #e7298a; font-size: 0.7em;"}
GLORIA:

- global change
- climate change
- time series analysis
- trend analysis
:::

::: {.absolute top=49% left=29% style="background-color: rgba(25, 25, 25, .7); border-radius: 10px; font-size: 1.3em;"}
\ \ Digital Landscapes \ \ 
:::

:::
# Learnings {.smaller .scrollable}

::: {.panel-tabset}

### Workflow

Adhering to best practices in scientific computing^[Wilson G, Aruliah DA, Brown CT, Chue Hong NP, Davis M, Guy RT, et al. (2014): Best Practices for Scientific Computing. *PLoS Biol.* 12(1): e1001745. [doi:10.1371/journal.pbio.1001745](https://doi.org/10.1371/journal.pbio.1001745).] ^[Wilson G, Bryan J, Cranston K, Kitzes J, Nederbragt L, Teal TK (2017): Good enough practices in scientific computing. *PLoS Comput. Biol.* 13(6): e1005510. [doi:10.1371/journal.pcbi.1005510](https://doi.org/10.1371/journal.pcbi.1005510).] and management of research projects:

- Project management via [issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html){preview-link="true"} (Kanban)
- Version control
- Reproducibility
- Collaborative development

### Open Science

Publish:

- Repositories^[[GitLab: Rexthor/debris-flow-trends-zermatt](https://gitlab.com/Rexthor/debris-flow-trends-zermatt)] ^[[GitLab: Rexthor/insar-early-warning](https://gitlab.com/Rexthor/insar-early-warning)]
- Code supplements^[[doi:10.1371/journal.pone.0233551.s008](https://doi.org/10.1371/journal.pone.0233551.s008)]
- Data^[Schlögl, M., Heiser, M., Scheidl, C. & Fuchs, S. (2021): Frequency, magnitude and seasonality of damage-inducing torrential events in Austria (1962-2017). *PANGAEA*. [doi:10.1594/PANGAEA.927584](https://doi.org/10.1594/PANGAEA.927584).]
- Additional visualization^[Schlögl, M. (2022): Vertical bridge deformation of Seitenhafenbrücke (Vienna, Austria) based on Sentinel-1 InSAR (PSI) and in-situ measurements. [doi:10.6084/m9.figshare.20035364](https://doi.org/10.6084/m9.figshare.20035364).]

### Transferability {.smaller}

::: columns
::: {.column width="50%"}
::: {style="text-align: center; font-size: 0.75em"}
[doi:10.1016/j.aap.2017.02.001](https://doi.org/10.1016/j.aap.2017.02.001){preview-link="true"}
:::

![](./img/traffic_count_clusters.png)
:::

::: {.column width="50%"}
::: {style="text-align: center; font-size: 0.75em"}
[doi:10.1016/j.isprsjprs.2020.12.001](https://doi.org/10.1016/j.isprsjprs.2020.12.001){preview-link="true"}
:::

![](https://ars.els-cdn.com/content/image/1-s2.0-S0924271620303324-gr7_lrg.jpg)
:::
:::

:::


# Contact {.smaller background-image="https://photojournal.jpl.nasa.gov/jpeg/PIA23645.jpg"}

::: {style="margin-top: 240px;"}

Matthias Schlögl [![](https://upload.wikimedia.org/wikipedia/commons/0/06/ORCID_iD.svg){width="24" height="24"}](https://orcid.org/0000-0002-4357-523X)

[matthias.schloegl@zamg.ac.at](mailto:matthias.schloegl@zamg.ac.at)

Division Data - Methods - Modelling

Department for Earth Observation and Geoinformation

Zentralanstalt für Meteorologie und Geodynamik, Wien
:::

::: footer
Slides: [**GitLab**](https://gitlab.com/Rexthor/digital-landscapes)
:::


# References {.smaller .scrollable}

::: {.panel-tabset}

### General
- Images are published under CC-BY, unless stated otherwise.
- Most images are taken from scientific publications of the author and are CC-BY in case of Open Access publications. The copyright to all other images lies with the respective publishing outlets.
- Potentially copyright-protected material is used under fair use conditions.
- No copyright infringement is intended.

### Images {style="font-size: 0.7em;"}

- Tyler Morgan-Wall (2022): [**rayshader**](https://www.rayshader.com/).
- MaximKartashev (2010): **Vue d'artiste de la Montagne Solitaire**. Public domain, via [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:LonelyMountain.jpg).
- ESA (2021): [**La Palma as captured by Copernicus Sentinel-2**](https://www.esa.int/ESA_Multimedia/Images/2021/10/La_Palma_as_captured_by_Copernicus_Sentinel-2). Contains modified Copernicus Sentinel data (2021), processed by ESA, [CC BY-SA 3.0 IGO](http://www.esa.int/spaceinvideos/Terms_and_Conditions).
- NASA (1976): **Color image of southern Argyre Basin and the crater Galle at left, with the thin atmosphere of Mars visible on the horizon.** Public domain, via [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Mars_atmosphere.jpg).
- Grivet, B.: (2014): [Caradhras](https://www.artstation.com/artwork/qAENy).
- Jürgele (2013): **Felssturz auf der Felbertauern Straße (14. Mai 2013)**. [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0), via [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Felbertauern_Felssturz.JPG).
- SWR (2021): **Flut-Katastrophe in Rheinland-Pfalz - Kreis Ahrweiler: Jetzt 117 Tote offiziell bestätigt**. [SWR Aktuell](https://www.swr.de/swraktuell/rheinland-pfalz/aufraeumen-nach-dem-hochwasser-100.html).
- Nefzger, A. / FAZ (2013): **Straßenschäden: Explodierender Beton**. Frankfurter Allgemeine / [FAZ.NET](https://www.faz.net/aktuell/gesellschaft/ungluecke/strassenschaeden-explodierender-beton-12294568.html).
- Bezirksblatt Imst (2013): **Pitztalstraße war gesperrt: Lawine ging auf die Pitztalstraße ab**. [Bezirksblatt Imst](https://www.meinbezirk.at/imst/c-lokales/lawine-ging-auf-die-pitztalstrasse-ab_a4467276). Image by [ZOOM.Tirol](https://zoom.tirol/)

:::
